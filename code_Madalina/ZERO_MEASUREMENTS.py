def ZERO_MEASUREMENTS(df, feature):
    
    # Import useful libraries       
    import pandas as pd  
    import numpy as np
    from national_holidays import get_national_holidays
    from datetime import timedelta   

    #Reset index
    df=df.reset_index(drop=True)  
    
    #Sort values by id              
    df=df.sort_values(by=['id'])   
    
    # Create a column specifying the day of the week for each date
    df['dia_semana']=df['fec'].dt.dayofweek
    
    # Import national holidays
    national_holidays=get_national_holidays()
     
    # Categorize data in 6 types L=0, T=1, S=2, LH=3, TH=4, SH=5
    df['tipo_dia']=0
    df.loc[df.dia_semana==5,'tipo_dia']=1
    df.loc[df.dia_semana==6,'tipo_dia']=2
    df.loc[(df.tipo_dia==0) & (df.fec.isin(national_holidays)),'tipo_dia']=3
    df.loc[(df.tipo_dia==1) & (df.fec.isin(national_holidays)),'tipo_dia']=4
    df.loc[(df.tipo_dia==2) & (df.fec.isin(national_holidays)),'tipo_dia']=5     
    
    # Keep only laborable days
    df=df[df.tipo_dia<1]
           
    # Create db with the most recent date for each ID, tramo and type of day
    df_dates = pd.DataFrame([])
    df_dates['fec_fin'] = df.groupby(['id','tramo'])['fec'].max()
    df_dates['fec_ini'] = df.groupby(['id','tramo'])['fec'].min()
    
    # Last numbers of days to get number of zero measurements
    num_days=[ 30, 60, 90]

    for d in num_days:        
        df_dates['fec_last_%s_days_ago' %d]=df_dates.fec_fin-timedelta(days=(d-1))

    # Join df_dates with the main dataframe
    df=df.join(df_dates, on=['id','tramo'], how='left')
        
    ###########################################################################
    ##################  CREATE TABLE WITH FEATURES   ##########################
    ###########################################################################   
    # Create the database which contains the average number oF std for each ID   
    zero_meas_db=pd.DataFrame([])
    
    zero_meas_db['id']=df_dates.index.get_level_values('id')
    
    zero_meas_db['tramo']=df_dates.index.get_level_values('tramo')   
        
    zero_meas_db.index=df_dates.index
    
    for d in num_days:
        zero_meas_db['NumZeroMeas_%s_days_%s' %(d,feature)]=np.empty(
                                                      len(zero_meas_db))*np.nan       

   
    for d in num_days:
        mask=(df.fec>=df['fec_last_%s_days_ago' %d])&(
                         df['fec_last_%s_days_ago' %d]>=df.fec_ini)
        count_zero=df[mask].groupby(['id','tramo'])[feature].apply(lambda x: x[x==0].shape[0])
        zero_meas_db.loc[count_zero.index,
                         'NumZeroMeas_%s_days_%s' %(d,feature)]=count_zero
    
    return(zero_meas_db)       