###############################################################################
#########  READ THE CHUNK CSV FILE AND JOIN IT WITH THE TRAMOS DATABASE #######
###############################################################################
def JOIN_TRAMOS_CHUNKS(path_ch,path_tramos,name_ch,
                       feature_to_extract,number_chunk, overlap):

    def lookup(s):
        '''
        This is an extremely fast approach to datetime parsing. For large data,
        the same dates are often repeated. Rather than re-parse these, we store
        all unique dates, parse them, and use a lookup to convert all dates.
        '''
        dates={date:pd.to_datetime(date,format='%d/%m/%Y') for date in s.unique()}
        return s.apply(lambda v: dates[v])

    # Import useful packages
    import pandas as pd

    #Define the path for the tramos and daily curves database and the time range
    if overlap:
        path_tramos=path_tramos+'/id_tramos_con_solape_%s.csv' %number_chunk
    else:
        path_tramos=path_tramos+'/id_tramos_sin_solape_%s.csv' %number_chunk

    # Read tramos database
    df_tramos=pd.read_csv(path_tramos,sep=';')
    df_tramos['fecini']=pd.to_datetime(df_tramos['fecini'])
    df_tramos['fecfin']=pd.to_datetime(df_tramos['fecfin'])
    df_tramos=df_tramos.reset_index(drop=True) #not necessary, index is fine

    # Initialize empty data frame
    df=pd.DataFrame([])

    # Extract filenames
    filenames=[x for x in name_ch if '_'+number_chunk in x]
    fields=['id','fec',feature_to_extract]

    # Merge data frame for one chunk for all years
    for filename in filenames:
        df_year=pd.read_csv(path_ch+'/'+filename, sep=';', usecols=fields )
        df_year['fec']=lookup(df_year['fec'])
        # Drop duplicates
        df_year.drop_duplicates(subset=['id','fec'], keep='last', inplace=True)
        df=pd.concat([df, df_year])
        
    # Merge main dataframe with df_tramos
    df=pd.merge(df,df_tramos,on=['id'])
    
    # Extract the measurements within the tramos range
    df=df[(df.fec>=df.fecini)&(df.fec<=df.fecfin)]

    # Extract required features
    df=df[['id', 'fec', feature_to_extract, 'tramo']]

    # Sort IDs in ascending order
    df=df.sort_values(by=['id'])
        
    # Reset index
    df=df.reset_index(drop=True)

    return(df)
