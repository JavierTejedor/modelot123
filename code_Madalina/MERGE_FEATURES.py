###############################################################################
#####################    Merge all features together    #######################
###############################################################################

def MERGE_FEATURES(overlap=True, alarms=True, zscore_oob=True,
                   monthly_ratio=False, zero_meas=True, trends=True,
                   slope_5_windows=False, stationarity=False):

    # Import useful libraries
    import pandas as pd
    import os
    import numpy as np

    if overlap:

        if alarms==True:
            alarm_files=os.listdir('../../raw/features_madalina/alarms_db')
            alarm_files=[x for x in alarm_files if 'con' in x]
            alarm_db=pd.DataFrame([])
            for file in alarm_files:
                if os.stat('../../raw/features_madalina/alarms_db/'+file).st_size > 5:
                    alarm_chunk=pd.read_csv('../../raw/features_madalina/alarms_db/'+file)
                    alarm_db=pd.concat([alarm_db, alarm_chunk])
            X=alarm_db

        for column in X:
            if 'NumDaysFrom' in column:
                X.loc[X[column]==0, column]=np.nan

        if trends==True:
            trend_files=os.listdir('../../raw/features_madalina/trend_slope_db')
            trend_files=[x for x in trend_files if 'con' in x]
            trend_db=pd.DataFrame([])
            for file in trend_files:
                if os.stat('../../raw/features_madalina/trend_slope_db/'+file).st_size > 5:
                    trend_chunk=pd.read_csv('../../raw/features_madalina/trend_slope_db/'+file)
                    trend_db=pd.concat([trend_db, trend_chunk])
            trend_db=trend_db.drop(['trend'], axis=1)
            X=X.merge(trend_db,on=['id','tramo'])

        if zero_meas==True:
            zero_files=os.listdir('../../raw/features_madalina/zero_measurements')
            zero_files=[x for x in zero_files if 'con' in x]
            # Merge dataframe for AE
            zero_ae_db=pd.DataFrame([])
            zero_files_ae=[x for x in zero_files if 'ae' in x]
            for file in zero_files_ae:
                zero_chunk=pd.read_csv('../../raw/features_madalina/zero_measurements/'+file)
                zero_ae_db=pd.concat([zero_ae_db, zero_chunk])
            X=X.merge(zero_ae_db,on=['id','tramo'])

        if zscore_oob==True:
            zscore_files=os.listdir('../../raw/features_madalina/zscore_oob_db')
            zscore_files=[x for x in zscore_files if 'con' in x]
            # Merge dataframe for AE
            zscore_ae_db=pd.DataFrame([])
            zscore_files_ae=[x for x in zscore_files if 'ae' in x]
            for file in zscore_files_ae:
                zscore_chunk=pd.read_csv('../../raw/features_madalina/zscore_oob_db/'+file)
                zscore_ae_db=pd.concat([zscore_ae_db, zscore_chunk])
            # Merge dataframe for F1
            zscore_f1_db=pd.DataFrame([])
            zscore_files_f1=[x for x in zscore_files if 'f1' in x]
            for file in zscore_files_f1:
                zscore_chunk=pd.read_csv('../../raw/features_madalina/zscore_oob_db/'+file)
                zscore_f1_db=pd.concat([zscore_f1_db, zscore_chunk])
            # Merge dataframe for F2
            zscore_f2_db=pd.DataFrame([])
            zscore_files_f2=[x for x in zscore_files if 'f2' in x]
            for file in zscore_files_f2:
                zscore_chunk=pd.read_csv('../../raw/features_madalina/zscore_oob_db/'+file)
                zscore_f2_db=pd.concat([zscore_f2_db, zscore_chunk])
            # Merge dataframe for F3
            zscore_f3_db=pd.DataFrame([])
            zscore_files_f3=[x for x in zscore_files if 'f3' in x]
            for file in zscore_files_f3:
                zscore_chunk=pd.read_csv('../../raw/features_madalina/zscore_oob_db/'+file)
                zscore_f3_db=pd.concat([zscore_f3_db, zscore_chunk])
            # Merge dataframe for F4
            zscore_f4_db=pd.DataFrame([])
            zscore_files_f4=[x for x in zscore_files if 'f4' in x]
            for file in zscore_files_f4:
                zscore_chunk=pd.read_csv('../../raw/features_madalina/zscore_oob_db/'+file)
                zscore_f4_db=pd.concat([zscore_f4_db, zscore_chunk])
            # Merge dataframe for F5
            zscore_f5_db=pd.DataFrame([])
            zscore_files_f5=[x for x in zscore_files if 'f5' in x]
            for file in zscore_files_f5:
                zscore_chunk=pd.read_csv('../../raw/features_madalina/zscore_oob_db/'+file)
                zscore_f5_db=pd.concat([zscore_f5_db, zscore_chunk])
            X=X.merge(zscore_ae_db,on=['id','tramo'])
            X=X.merge(zscore_f1_db,on=['id','tramo'])
            X=X.merge(zscore_f2_db,on=['id','tramo'])
            X=X.merge(zscore_f3_db,on=['id','tramo'])
            X=X.merge(zscore_f4_db,on=['id','tramo'])
            X=X.merge(zscore_f5_db,on=['id','tramo'])

        if monthly_ratio==True:
            monthly_files=os.listdir('../../raw/features_madalina/monthly_ratio_db')
            monthly_files=[x for x in monthly_files if 'con' in x]
            # Merge dataframe for AE
            monthly_ae_db=pd.DataFrame([])
            monthly_files_ae=[x for x in monthly_files if 'ae' in x]
            for file in monthly_files_ae:
                monthly_chunk=pd.read_csv('../../raw/features_madalina/monthly_ratio_db/'+file)
                monthly_ae_db=pd.concat([monthly_ae_db, monthly_chunk])
            # Merge dataframe for F1
            monthly_f1_db=pd.DataFrame([])
            monthly_files_f1=[x for x in monthly_files if 'f1' in x]
            for file in monthly_files_f1:
                monthly_chunk=pd.read_csv('../../raw/features_madalina/monthly_ratio_db/'+file)
                monthly_f1_db=pd.concat([monthly_f1_db, monthly_chunk])
            # Merge dataframe for F2
            monthly_f2_db=pd.DataFrame([])
            monthly_files_f2=[x for x in monthly_files if 'f2' in x]
            for file in monthly_files_f2:
                monthly_chunk=pd.read_csv('../../raw/features_madalina/monthly_ratio_db/'+file)
                monthly_f2_db=pd.concat([monthly_f2_db, monthly_chunk])
            # Merge dataframe for F3
            monthly_f3_db=pd.DataFrame([])
            monthly_files_f3=[x for x in monthly_files if 'f3' in x]
            for file in monthly_files_f3:
                monthly_chunk=pd.read_csv('../../raw/features_madalina/monthly_ratio_db/'+file)
                monthly_f3_db=pd.concat([monthly_f3_db, monthly_chunk])
            # Merge dataframe for F4
            monthly_f4_db=pd.DataFrame([])
            monthly_files_f4=[x for x in monthly_files if 'f4' in x]
            for file in monthly_files_f4:
                monthly_chunk=pd.read_csv('../../raw/features_madalina/monthly_ratio_db/'+file)
                monthly_f4_db=pd.concat([monthly_f4_db, monthly_chunk])
            # Merge dataframe for F5
            monthly_f5_db=pd.DataFrame([])
            monthly_files_f5=[x for x in monthly_files if 'f5' in x]
            for file in monthly_files_f5:
                monthly_chunk=pd.read_csv('../../raw/features_madalina/monthly_ratio_db/'+file)
                monthly_f5_db=pd.concat([monthly_f5_db, monthly_chunk])
            X=X.merge(monthly_ae_db,on=['id','tramo'])
            X=X.merge(monthly_f1_db,on=['id','tramo'])
            X=X.merge(monthly_f2_db,on=['id','tramo'])
            X=X.merge(monthly_f3_db,on=['id','tramo'])
            X=X.merge(monthly_f4_db,on=['id','tramo'])
            X=X.merge(monthly_f5_db,on=['id','tramo'])
        
        if slope_5_windows:
            # Read slope 5 windows
            slope_files = os.listdir('slope_5_windows')
            slope = pd.DataFrame([])
            for file in slope_files:
              s = pd.read_csv('slope_5_windows/'+file)
              slope = pd.concat([slope, s], axis=0)            
            X = X.merge(slope, on=['id', 'tramo'], how='left')
        
        if stationarity:
            stationarity_files = os.listdir('stationarity_test')
            stat_30 = [x for x in stationarity_files if 'last_30_days' in x]
            stat_df_30 = pd.DataFrame([])
            for file in stat_30:
              stat_chunk = pd.read_csv('stationarity_test/'+file)
              stat_df_30 = pd.concat([stat_df_30, stat_chunk], axis=0)
            stat_90 = [x for x in stationarity_files if 'last_90_days' in x]
            stat_df_90 = pd.DataFrame([])
            for file in stat_90:
              stat_chunk = pd.read_csv('stationarity_test/'+file)
              stat_df_90 = pd.concat([stat_df_90, stat_chunk], axis=0)
            stat = stat_df_30.merge(stat_df_90, on=['id', 'tramo'], how='left')          
            X = X.merge(stat, on=['id', 'tramo'], how='left')
        
        # Write file to disk
        X.to_csv('../../raw/features_madalina/madalina_features_con_solape.csv', index=False)

    else:

        if alarms==True:
            alarm_files=os.listdir('../../raw/features_madalina/alarms_db')
            alarm_files=[x for x in alarm_files if 'sin' in x]
            alarm_db=pd.DataFrame([])
            for file in alarm_files:
                if os.stat('../../raw/features_madalina/alarms_db/'+file).st_size > 5:
                    alarm_chunk=pd.read_csv('../../raw/features_madalina/alarms_db/'+file)
                    alarm_db=pd.concat([alarm_db, alarm_chunk])
            X=alarm_db

        for column in X:
            if 'NumDaysFrom' in column:
                X[column][X[column]==0]=np.nan

        if trends==True:
            trend_files=os.listdir('../../raw/features_madalina/trend_slope_db')
            trend_files=[x for x in trend_files if 'sin' in x]
            trend_db=pd.DataFrame([])
            for file in trend_files:
                if os.stat('../../raw/features_madalina/trend_slope_db/'+file).st_size > 5:
                    trend_chunk=pd.read_csv('../../raw/features_madalina/trend_slope_db/'+file)
                    trend_db=pd.concat([trend_db, trend_chunk])
            trend_db=trend_db.drop(['trend'], axis=1)
            X=X.merge(trend_db,on=['id','tramo'])

        if zero_meas==True:
            zero_files=os.listdir('../../raw/features_madalina/zero_measurements')
            zero_files=[x for x in zero_files if 'sin' in x]
            # Merge dataframe for AE
            zero_ae_db=pd.DataFrame([])
            zero_files_ae=[x for x in zero_files if 'ae' in x]
            for file in zero_files_ae:
                zero_chunk=pd.read_csv('../../raw/features_madalina/zero_measurements/'+file)
                zero_ae_db=pd.concat([zero_ae_db, zero_chunk])
            X=X.merge(zero_ae_db,on=['id','tramo'])

        if zscore_oob==True:
            zscore_files=os.listdir('../../raw/features_madalina/zscore_oob_db')
            zscore_files=[x for x in zscore_files if 'sin' in x]
            # Merge dataframe for AE
            zscore_ae_db=pd.DataFrame([])
            zscore_files_ae=[x for x in zscore_files if 'ae' in x]
            for file in zscore_files_ae:
                zscore_chunk=pd.read_csv('../../raw/features_madalina/zscore_oob_db/'+file)
                zscore_ae_db=pd.concat([zscore_ae_db, zscore_chunk])
            # Merge dataframe for F1
            zscore_f1_db=pd.DataFrame([])
            zscore_files_f1=[x for x in zscore_files if 'f1' in x]
            for file in zscore_files_f1:
                zscore_chunk=pd.read_csv('../../raw/features_madalina/zscore_oob_db/'+file)
                zscore_f1_db=pd.concat([zscore_f1_db, zscore_chunk])
            # Merge dataframe for F2
            zscore_f2_db=pd.DataFrame([])
            zscore_files_f2=[x for x in zscore_files if 'f2' in x]
            for file in zscore_files_f2:
                zscore_chunk=pd.read_csv('../../raw/features_madalina/zscore_oob_db/'+file)
                zscore_f2_db=pd.concat([zscore_f2_db, zscore_chunk])
            # Merge dataframe for F3
            zscore_f3_db=pd.DataFrame([])
            zscore_files_f3=[x for x in zscore_files if 'f3' in x]
            for file in zscore_files_f3:
                zscore_chunk=pd.read_csv('../../raw/features_madalina/zscore_oob_db/'+file)
                zscore_f3_db=pd.concat([zscore_f3_db, zscore_chunk])
            # Merge dataframe for F4
            zscore_f4_db=pd.DataFrame([])
            zscore_files_f4=[x for x in zscore_files if 'f4' in x]
            for file in zscore_files_f4:
                zscore_chunk=pd.read_csv('../../raw/features_madalina/zscore_oob_db/'+file)
                zscore_f4_db=pd.concat([zscore_f4_db, zscore_chunk])
            # Merge dataframe for F5
            zscore_f5_db=pd.DataFrame([])
            zscore_files_f5=[x for x in zscore_files if 'f5' in x]
            for file in zscore_files_f5:
                zscore_chunk=pd.read_csv('../../raw/features_madalina/zscore_oob_db/'+file)
                zscore_f5_db=pd.concat([zscore_f5_db, zscore_chunk])
            X=X.merge(zscore_ae_db,on=['id','tramo'])
            X=X.merge(zscore_f1_db,on=['id','tramo'])
            X=X.merge(zscore_f2_db,on=['id','tramo'])
            X=X.merge(zscore_f3_db,on=['id','tramo'])
            X=X.merge(zscore_f4_db,on=['id','tramo'])
            X=X.merge(zscore_f5_db,on=['id','tramo'])


        if monthly_ratio==True:
            monthly_files=os.listdir('../../raw/features_madalina/monthly_ratio_db')
            monthly_files=[x for x in monthly_files if 'sin' in x]
            # Merge dataframe for AE
            monthly_ae_db=pd.DataFrame([])
            monthly_files_ae=[x for x in monthly_files if 'ae' in x]
            for file in monthly_files_ae:
                monthly_chunk=pd.read_csv('../../raw/features_madalina/monthly_ratio_db/'+file)
                monthly_ae_db=pd.concat([monthly_ae_db, monthly_chunk])
            # Merge dataframe for F1
            monthly_f1_db=pd.DataFrame([])
            monthly_files_f1=[x for x in monthly_files if 'f1' in x]
            for file in monthly_files_f1:
                monthly_chunk=pd.read_csv('../../raw/features_madalina/monthly_ratio_db/'+file)
                monthly_f1_db=pd.concat([monthly_f1_db, monthly_chunk])
            # Merge dataframe for F2
            monthly_f2_db=pd.DataFrame([])
            monthly_files_f2=[x for x in monthly_files if 'f2' in x]
            for file in monthly_files_f2:
                monthly_chunk=pd.read_csv('../../raw/features_madalina/monthly_ratio_db/'+file)
                monthly_f2_db=pd.concat([monthly_f2_db, monthly_chunk])
            # Merge dataframe for F3
            monthly_f3_db=pd.DataFrame([])
            monthly_files_f3=[x for x in monthly_files if 'f3' in x]
            for file in monthly_files_f3:
                monthly_chunk=pd.read_csv('../../raw/features_madalina/monthly_ratio_db/'+file)
                monthly_f3_db=pd.concat([monthly_f3_db, monthly_chunk])
            # Merge dataframe for F4
            monthly_f4_db=pd.DataFrame([])
            monthly_files_f4=[x for x in monthly_files if 'f4' in x]
            for file in monthly_files_f4:
                monthly_chunk=pd.read_csv('../../raw/features_madalina/monthly_ratio_db/'+file)
                monthly_f4_db=pd.concat([monthly_f4_db, monthly_chunk])
            # Merge dataframe for F5
            monthly_f5_db=pd.DataFrame([])
            monthly_files_f5=[x for x in monthly_files if 'f5' in x]
            for file in monthly_files_f5:
                monthly_chunk=pd.read_csv('../../raw/features_madalina/monthly_ratio_db/'+file)
                monthly_f5_db=pd.concat([monthly_f5_db, monthly_chunk])
            X=X.merge(monthly_ae_db,on=['id','tramo'])
            X=X.merge(monthly_f1_db,on=['id','tramo'])
            X=X.merge(monthly_f2_db,on=['id','tramo'])
            X=X.merge(monthly_f3_db,on=['id','tramo'])
            X=X.merge(monthly_f4_db,on=['id','tramo'])
            X=X.merge(monthly_f5_db,on=['id','tramo'])

        X.to_csv('../../raw/features_madalina/madalina_features_sin_solape.csv', index=False)

    
        
