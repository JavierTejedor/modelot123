def TREND_SLOPE(df):
    
    # Import useful libraries       
    import pandas as pd  
    import numpy as np
   
    #Reset index
    df=df.reset_index(drop=True)  
       
    # Get maximum consumption
    ae_max=df.groupby(['id','tramo'])['ae'].max().rename('ae_max')
    
    df=df.join(ae_max, on=['id','tramo'])
    
    df['ae_normalized']=df.ae/df.ae_max
    
    # Sort values by id              
    df=df.sort_values(by=['id', 'fec'])     
    
    ###########################################################################
    # Linear regression 
    ###########################################################################
    from scipy import stats
    
    df_lr=pd.DataFrame([])
    df_lr=df.groupby(['id','tramo'])['ae_normalized'].apply(
            lambda x: stats.linregress(range(0,len(x)),x))
            
    df_trend=pd.DataFrame([x[:][0] for x in df_lr], columns=['slope'])
    df_trend['trend']=np.nan
    df_trend.loc[(df_trend.slope<0),'trend']=-1
    df_trend.loc[(df_trend.slope>0),'trend']=1
    df_trend.loc[(df_trend.slope==0),'trend']=0
    df_trend['id']=df_lr.index.get_level_values('id')
    df_trend['tramo']=df_lr.index.get_level_values('tramo')
    
    
    return(df_trend)       