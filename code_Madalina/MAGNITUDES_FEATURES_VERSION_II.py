def get_features_magnitudes(window='first', 
                          #  actual=True,concentrator=True, merged=True,
                            path_tramos='../../raw/id_tramos_chunked',
                          #  path_actual='../../raw/raw_magnitudes_instantaneas_actualg/',
                          #  path_concentrator='../../raw/raw_magnitudes_instantaneas_cs/',
                            path_mi='C:/Proyectos/Modelo_DetectorAF_Tipos123/raw/raw_magnitudesIns/',
                            path_files='../../raw/features_magnitudes',
                            path_ricardo_features='../../raw/features_cifdni'):

    ###########################################################################
    # Get features from ActualG snapshots
    ###########################################################################
    # Import useful libraries
    import pandas as pd
    import numpy as np
    import warnings
    warnings.filterwarnings('ignore')

    # Read id_tramos database
    import os
    name_tramos=os.listdir(path_tramos)
    name_tramos=[x for x in name_tramos if 'con' in x]

    # Read and merge tramos database
    tramos=pd.DataFrame([])
    for file in name_tramos:
        df_tramos=pd.read_csv(path_tramos+'/'+file, sep=';')
        df_tramos['fecini']=pd.to_datetime(df_tramos['fecini'])
        df_tramos['fecfin']=pd.to_datetime(df_tramos['fecfin'])
        tramos=pd.concat([tramos, df_tramos])
        tramos=tramos[['id','tramo','fecini','fecfin','TEXPEDIE']].reset_index(drop=True)

    # Make path to write the files
    os.makedirs(path_files, exist_ok=True)

    # Step 1: Read csv file
    mag=pd.read_csv(path_mi+'magnitudesInstantaneas.csv',decimal=',', index_col=0)
    mag.rename(columns={'cs_id_pm':'id'}, inplace=True)

    # Remove measurements without an id
    mag=mag[~mag.id.isnull()]

    # Change dtype for some features
    mag['id']=mag['id'].astype(int)
    mag['AE']=mag['AE'].astype(float)

    # Drop duplicates
    mag.drop_duplicates(subset=['id','FECHALECTURA'],inplace=True)

    # Get the date from timestamp
    mag['DAYLECTURE']=[x.split(' ')[0] for x in mag['FECHALECTURA']]

    # Get the hour and minute from timestamp
    mag['HOURLECTURE']=[int(x.split(' ')[1].split(':')[0]) for x in mag['FECHALECTURA']]
    mag['MINUTELECTURE']=[int(x.split(' ')[1].split(':')[1]) for x in mag['FECHALECTURA']]

    # Drop 'FECHALECTURA'
    mag.drop('FECHALECTURA', axis=1, inplace=True)

    # Function to parse datetimes more efficiently
    def lookup(s):
        dates={date:pd.to_datetime(date,format='%d/%m/%Y') for date in s.unique()}
        return s.apply(lambda v: dates[v])

    # From string to datetime
    mag['DAYLECTURE']=lookup(mag['DAYLECTURE'])

    # Create windows
    if window=='first':
        mag=mag[(mag['HOURLECTURE']>=9)&(mag['HOURLECTURE']<=18)]
    elif window=='second':
        mag=mag[(mag['HOURLECTURE']>=19)&(mag['HOURLECTURE']<=22)]
    elif window=='third':
        mag=mag[(mag['HOURLECTURE']==23)|(mag['HOURLECTURE']<=8)]

    # Merge snapshots database with tramos database
    mag=mag.merge(tramos, on='id')
    mag=mag[(mag['DAYLECTURE']>=mag['fecini'])&(mag['DAYLECTURE']<=mag['fecfin'])]

    # Sort values by daylecture
    mag.sort_values(by=['id','tramo','DAYLECTURE'], ascending=False, inplace=True)

    # Get the last three snapshots
    mag=mag.groupby(['id','tramo']).head(n=3)

    # Zero current found
    mag['zero_current']=0
    mag.loc[(mag['I1']==0)|(
                mag['I2']==0)|(mag['I3']==0),'zero_current']=1

    mag.sort_values(by=['id','tramo','zero_current'],ascending=True, inplace=True)

    # Get best snapshot
    mag=mag.groupby(['id','tramo']).head(n=1)

    # Compute the neutral current
    theta=[np.radians(0), np.radians(-120), np.radians(120)]
    for i in [1,2,3]:
        mag['PHI%d' %i]=np.nan
        mag.loc[(mag['PA%d' %i]>=0)&(mag['PR%d' %i]>=0),'PHI%s' %i]=theta[i-1]-np.arccos(
        mag.loc[(mag['PA%d' %i]>=0)&(mag['PR%d' %i]>=0),'COS%s' %i])
        mag.loc[(mag['PA%d' %i]<0)&(mag['PR%d' %i]>0),'PHI%s' %i]=theta[i-1]+np.arccos(
        mag.loc[(mag['PA%d' %i]<0)&(mag['PR%d' %i]>0),'COS%s' %i])+np.radians(180)
        mag.loc[(mag['PA%d' %i]<0)&(mag['PR%d' %i]<0),'PHI%s' %i]=theta[i-1]-np.arccos(
        mag.loc[(mag['PA%d' %i]<0)&(mag['PR%d' %i]<0),'COS%s' %i])+np.radians(180)
        mag.loc[(mag['PA%d' %i]>0)&(mag['PR%d' %i]<0),'PHI%s' %i]=theta[i-1]+np.arccos(
        mag.loc[(mag['PA%d' %i]>0)&(mag['PR%d' %i]<0),'COS%s' %i])

        mag['complex_I%d' %i]=(mag['I%d' %i]*np.cos(mag['PHI%d' %i])+
                            mag['I%d' %i]*np.sin(mag['PHI%d' %i])*1j)

    mag['neutral_current']=(-mag['complex_I1']-mag['complex_I2']
                                    -mag['complex_I3'])

    mag['neutral_current_ratio_min']=(np.abs(-mag['complex_I1']-mag['complex_I2']
    -mag['complex_I3']))/(np.abs(mag[['I1','I2','I3']]).min(axis=1))

    mag['neutral_current_ratio_max']=(np.abs(-mag['complex_I1']-mag['complex_I2']
    -mag['complex_I3']))/(np.abs(mag[['I1','I2','I3']]).max(axis=1))

    mag['neutral_current_angle']=np.arctan(
    np.imag(mag['neutral_current'])/mag['neutral_current'].real)

    mag['neutral_current']=np.abs(mag['neutral_current'])

    mag.loc[(mag['zero_current']==1),['neutral_current_ratio_min',
                                    'neutral_current',
                                    'neutral_current_angle']]=np.nan

    # Select relevant measurements and features
    mag_filtered=mag[['AS','AE','R1',
                            'R2','R3','R4','I1','I2','I3','COS1',
                            'COS2','COS3','V1','V2','V3','PA1',
                            'PA2','PA3','PR1','PR2','PR3',
                            'neutral_current_angle',
                            'neutral_current_ratio_min',
                            'neutral_current_ratio_max',
                            'DAYLECTURE',
                            'HOURLECTURE',
                            'id','tramo']]

    # Compute features
    # Initialize an empty dataframe for the gigantic dataframe of the magnitude features
    features_mag=pd.DataFrame([])
    features_mag['id']= mag_filtered['id']
    features_mag['tramo']= mag_filtered['tramo']

    measurements={'V':'voltage','I':'current','COS':'power_factor',
                  'PA':'active_power','PR':'reactive_power'}
    phases=[1,2,3]

    # Raw measurements
    for m in measurements:
        for p in phases:
            features_mag['raw_%s%d' %(m,p)]= mag_filtered['%s%d' %(m,p)]

    # Relative difference between max and min
    for m in ['V','COS']:
        features_mag['rel_diff_max_min_%s' %(measurements[m])]=((features_mag[['raw_%s1' %m,'raw_%s2' %m,'raw_%s3' %m]].max(axis=1)-features_mag[
                                                ['raw_%s1' %m,'raw_%s2' %m,'raw_%s3' %m]].min(axis=1))/
                                                (features_mag[['raw_%s1' %m,'raw_%s2' %m,'raw_%s3' %m]].max(axis=1)))

    # Yes/No features
    # Zero voltages
    features_mag['zero_voltage_found']=0
    features_mag.loc[( mag_filtered.V1<=0)|( mag_filtered.V2<=0)|( mag_filtered.V3<=0),'zero_voltage_found']=1
    features_mag['zero_voltage_phase_one']=0
    features_mag.loc[( mag_filtered.V1<=0),'zero_voltage_phase_one']=1
    features_mag['zero_voltage_phase_two']=0
    features_mag.loc[( mag_filtered.V2<=0),'zero_voltage_phase_two']=1
    features_mag['zero_voltage_phase_three']=0
    features_mag.loc[( mag_filtered.V3<=0),'zero_voltage_phase_three']=1

    #Zero power factors
    features_mag['zero_power_factor_found']=0
    features_mag.loc[((features_mag['raw_COS1']==0)&(features_mag['raw_COS2']!=0)&(
                        features_mag['raw_COS3']!=0))|((features_mag['raw_COS2']==0)&(
                        features_mag['raw_COS1']!=0)&(
                        features_mag['raw_COS3']!=0))|((features_mag['raw_COS3']==0)&(
                        features_mag['raw_COS2']!=0)&(
                        features_mag['raw_COS1']!=0)),'zero_power_factor_found']=1
    features_mag['zero_power_factor_phase_one']=0
    features_mag.loc[(features_mag['raw_COS1']==0)&(features_mag['raw_COS2']!=0)&(
                        features_mag['raw_COS3']!=0),'zero_power_factor_phase_one']=1
    features_mag['zero_power_factor_phase_two']=0
    features_mag.loc[(features_mag['raw_COS2' ]==0)&(features_mag['raw_COS1']!=0)&(
                        features_mag['raw_COS3']!=0),'zero_power_factor_phase_two']=1
    features_mag['zero_power_factor_phase_three']=0
    features_mag.loc[(features_mag['raw_COS3']==0)&(features_mag['raw_COS2']!=0)&(
                        features_mag['raw_COS1']!=0),'zero_power_factor_phase_three']=1

    measurements={'I':'current','PA':'active_power'}

    # Add here more variables if needed
    for m in ['I']:
        features_mag['negative_%s_found' %(measurements[m])]=0
        features_mag.loc[((features_mag['raw_%s1' %m]<0)&
                            (features_mag['raw_%s2' %m]>0)&(
                            features_mag['raw_%s3' %m]>0))|((features_mag['raw_%s2' %m]<0)&
                            (features_mag['raw_%s1' %m]>0)&(
                            features_mag['raw_%s3' %m]>0))|((features_mag['raw_%s3' %m]<0)&
                            (features_mag['raw_%s2' %m]>0)&(
                            features_mag['raw_%s1' %m]>0)),'negative_%s_found' %(measurements[m])]=1
        features_mag['negative_%s_phase_one' %(measurements[m])]=0
        features_mag.loc[(features_mag['raw_%s1' %m]<0)&(features_mag['raw_%s2' %m]>0)&(
                            features_mag['raw_%s3' %m]>0),'negative_%s_phase_one' %(measurements[m])]=1
        features_mag['negative_%s_phase_two' %(measurements[m])]=0
        features_mag.loc[(features_mag['raw_%s2' %m]<0)&(features_mag['raw_%s1' %m]>0)&(
                            features_mag['raw_%s3' %m]>0),'negative_%s_phase_two'  %(measurements[m])]=1
        features_mag['negative_%s_phase_three' %(measurements[m])]=0
        features_mag.loc[(features_mag['raw_%s3' %m]<0)&(features_mag['raw_%s2' %m]>0)&(
                            features_mag['raw_%s1' %m]>0),'negative_%s_phase_three' %(measurements[m])]=1

    # Add here for more variables if needed
    for m in ['PA']:
        features_mag['negative_%s_found' %(measurements[m])]=0
        features_mag.loc[((features_mag['raw_%s1' %m]<0)&(features_mag['raw_%s2' %m]>0)&(
                            features_mag['raw_%s3' %m]>0))|((features_mag['raw_%s2' %m]<0)&(features_mag['raw_%s1' %m]>0)&(
                            features_mag['raw_%s3' %m]>0))|((features_mag['raw_%s3' %m]<0)&(features_mag['raw_%s2' %m]>0)&(
                            features_mag['raw_%s1' %m]>0)),'negative_%s_found' %(measurements[m])]=1
        features_mag['negative_%s_phase_one' %(measurements[m])]=0
        features_mag.loc[(features_mag['raw_%s1' %m]<0)&(features_mag['raw_%s2' %m]>0)&(
                            features_mag['raw_%s3' %m]>0),'negative_%s_phase_one' %(measurements[m])]=1
        features_mag['negative_%s_phase_two' %(measurements[m])]=0
        features_mag.loc[(features_mag['raw_%s2' %m]<0)&(features_mag['raw_%s1' %m]>0)&(
                            features_mag['raw_%s3' %m]>0),'negative_%s_phase_two'  %(measurements[m])]=1
        features_mag['negative_%s_phase_three' %(measurements[m])]=0
        features_mag.loc[(features_mag['raw_%s3' %m]<0)&(features_mag['raw_%s2' %m]>0)&(
                            features_mag['raw_%s1' %m]>0),'negative_%s_phase_three' %(measurements[m])]=1


    # POSIBILES EQUIPOS MANIPULADOS
    # Case I: R1>AE & PR1+PR2+PR3>0 & PR1+PR2+PR3>PA1+PA2+PA3
    features_mag['equipos_manipulados_I']=0

    features_mag.loc[(( mag_filtered['R1'].astype(float)> mag_filtered['AE'].astype(float)
                     ) & (( mag_filtered['PR1']+ mag_filtered['PR2']+
                    mag_filtered['PR3'])>0)&(( mag_filtered['PR1']+ mag_filtered['PR2']+
                    mag_filtered['PR3'])>( mag_filtered['PA1']+
                    mag_filtered['PA2']+ mag_filtered['PA3']))),'equipos_manipulados_I']=1

    features_mag['equipos_manipulados_II']=0

    features_mag.loc[(( mag_filtered['R4'].astype(float)> mag_filtered['AE'].astype(float)
                     ) & (( mag_filtered['PR1']+ mag_filtered['PR2']+
                    mag_filtered['PR3'])<0)&(( mag_filtered['PR1']+ mag_filtered['PR2']+
                    mag_filtered['PR3'])>( mag_filtered['PA1']+
                    mag_filtered['PA2']+ mag_filtered['PA3']))),'equipos_manipulados_II']=1

    features_mag['positive_AS']=0
    features_mag['positive_R2']=0
    features_mag['positive_R3']=0
    features_mag.loc[mag_filtered['AS']>0,'positive_AS']=1
    features_mag.loc[mag_filtered['R2']>0,'positive_R2']=1
    features_mag.loc[mag_filtered['R3']>0,'positive_R3']=1


    # Puente intensidad
    features_mag['puente_intensidad_fase_1'] = 0
    features_mag['puente_intensidad_fase_2'] = 0
    features_mag['puente_intensidad_fase_3'] = 0
    features_mag.loc[(features_mag['raw_I1']==0)&(
      features_mag['raw_COS1']==0)&(features_mag['raw_I2']!=0)&(
      features_mag['raw_COS2']!=0)&(features_mag['raw_I3']!=0)&(
      features_mag['raw_COS3']!=0), 'puente_intensidad_fase_1'] = 1
    features_mag.loc[(features_mag['raw_I2']==0)&(
      features_mag['raw_COS2']==0)&(features_mag['raw_I1']!=0)&(
      features_mag['raw_COS1']!=0)&(features_mag['raw_I3']!=0)&(
      features_mag['raw_COS3']!=0), 'puente_intensidad_fase_2'] = 1
    features_mag.loc[(features_mag['raw_I3']==0)&(
      features_mag['raw_COS3']==0)&(features_mag['raw_I1']!=0)&(
      features_mag['raw_COS1']!=0)&(features_mag['raw_I2']!=0)&(
      features_mag['raw_COS2']!=0), 'puente_intensidad_fase_3'] = 1

    # Check for generating customers
    # Id's that are generating
    cod_persona = pd.read_csv(path_ricardo_features+'/cod_persona_ConSolape.csv', sep=';')
    cups_generation = pd.read_excel('../../raw/raw_magnitudes_cups_generacion/cups_generation.xlsx')
    gen_ids = (cod_persona[cod_persona['sce_id_cups22pf']
      .isin(cups_generation['CUPS largo consumo'])])
    # Set puente intensidad to 0 for id's that are generating
    features_mag.loc[features_mag.id.isin(gen_ids['cs_id_pm']),
      'puente_intensidad_fase_1'] = 0
    features_mag.loc[features_mag.id.isin(gen_ids['cs_id_pm']),
      'puente_intensidad_fase_2'] = 0
    features_mag.loc[features_mag.id.isin(gen_ids['cs_id_pm']),
      'puente_intensidad_fase_3'] = 0

    raw_features=[x for x in features_mag.columns if 'raw' in x]

    features_mag.drop(raw_features,axis=1,inplace=True)

    features_mag.replace([np.inf, -np.inf], np.nan,inplace=True)

    features_mag['neutral_current_angle']= mag_filtered['neutral_current_angle']
    features_mag['neutral_current_ratio_min']= mag_filtered[
                                            'neutral_current_ratio_min']
    features_mag['neutral_current_ratio_max']= mag_filtered[
                                            'neutral_current_ratio_max']


    features_mag.to_csv(path_files+'/features_magnitudes_merged_last_%s_conSolape.csv' %window, index=False)



get_features_magnitudes(window='first')
get_features_magnitudes(window='second')
get_features_magnitudes(window='third')
get_features_magnitudes(window='no_window')


#get_features_magnitudes(window='first', actual=False,
#                             concentrator=True, merged=False,
#                             path_actual='', path_concentrator='')
#get_features_magnitudes(window='second', actual=False,
#                             concentrator=True, merged=False,
#                             path_actual='', path_concentrator='')
#get_features_magnitudes(window='third', actual=False,
#                             concentrator=True, merged=False,
#                             path_actual='', path_concentrator='')
#get_features_magnitudes(window='no_window', actual=False,
#                             concentrator=True, merged=False,
#                             path_actual='', path_concentrator='')


#get_features_magnitudes(window='first', actual=True,
#                             concentrator=True, merged=True,
#                             path_actual='', path_concentrator='')
#get_features_magnitudes(window='second', actual=True,
#                             concentrator=True, merged=True,
#                             path_actual='', path_concentrator='')
#get_features_magnitudes(window='third', actual=True,
#                             concentrator=True, merged=True,
#                             path_actual='', path_concentrator='')
#get_features_magnitudes(window='no_window', actual=True,
#                             concentrator=True, merged=True,
#                             path_actual='', path_concentrator='')
