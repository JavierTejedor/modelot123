def Z_SCORE_OOB(df,feature):
    
    # Import useful libraries       
    import pandas as pd  
    import numpy as np
    from national_holidays import get_national_holidays
    from datetime import timedelta   

    #Reset index
    df=df.reset_index(drop=True)  
    
    #Sort values by id              
    df=df.sort_values(by=['id'])   
    
    # Create a column specifying the day of the week for each date
    df['dia_semana']=df['fec'].dt.dayofweek
    
    # Import national holidays
    national_holidays=get_national_holidays()
     
    # Categorize data in 6 types L=0, T=1, S=2, LH=3, TH=4, SH=5
    df['tipo_dia']=0
    df.loc[df.dia_semana==5,'tipo_dia']=1
    df.loc[df.dia_semana==6,'tipo_dia']=2
    df.loc[(df.tipo_dia==0) & (df.fec.isin(national_holidays)),'tipo_dia']=3
    df.loc[(df.tipo_dia==1) & (df.fec.isin(national_holidays)),'tipo_dia']=4
    df.loc[(df.tipo_dia==2) & (df.fec.isin(national_holidays)),'tipo_dia']=5     
    
    # Get rid of the LD, TH, SH measurements
    df=df[df.tipo_dia<3]
          
    # Create db with the most recent date for each ID, tramo and type of day
    df_dates = pd.DataFrame([])
    df_dates['fec_fin'] = df.groupby(['id','tramo'])['fec'].max()
    df_dates['fec_ini'] = df.groupby(['id','tramo'])['fec'].min()
    
    # Last numbers of days to get the average
    num_days=[15, 30, 45, 60, 90]

    for d in num_days:        
        df_dates['fec_last_%s_days_ago' %d]=df_dates.fec_fin-timedelta(days=(d-1))

    # Join df_dates with the main dataframe
    df=df.join(df_dates, on=['id','tramo'], how='left')
        
    ###########################################################################
    ##################  CREATE TABLE WITH FEATURES   ##########################
    ###########################################################################
    # Type of days to compute the average L=0, T=1, S=2
    type_days={'L':0,'T':1,'S':2}
    
    # Create the database which contains the average number oF std for each ID   
    zscore_db=pd.DataFrame([])
    
    zscore_db['id']=df_dates.index.get_level_values('id')
    
    zscore_db['tramo']=df_dates.index.get_level_values('tramo')   
        
    zscore_db.index=df_dates.index
    
    # Initialize empty columns
    for t in type_days.keys():
        for d in num_days:
            zscore_db['AvgZscore_OOB_%s_days_%s_%s' %(d,t,feature)]=np.empty(
                                                          len(zscore_db))*np.nan
        

    # Compute the average number of standard deviations away from the mean,
    # for each type of day and tramo of each ID
    for t in type_days.keys():
        for d in num_days:
            previous=pd.DataFrame([])
            mask_previous=(df.fec<df['fec_last_%s_days_ago' %d])&(
                             df['fec_last_%s_days_ago' %d]>=df.fec_ini)&(
                             df.tipo_dia==type_days[t])
            previous['pre_mean']=df[mask_previous].groupby(
                                ['id','tramo'])[feature].mean()
            previous['pre_std']=df[mask_previous].groupby(
                                ['id','tramo'])[feature].std()
                                
            df=df.join(previous,on=['id','tramo'],how='left')
            
            df['NumStdFromMean']=(df[feature]-df.pre_mean)/df.pre_std
           
            mask_last=(df.fec>=df['fec_last_%s_days_ago' %d])&(
                             df['fec_last_%s_days_ago' %d]>=df.fec_ini)&(
                             df.tipo_dia==type_days[t])            
            
            avg_std=df[mask_last].groupby(['id','tramo'])['NumStdFromMean'].mean()
            
            df=df.drop(['pre_mean','pre_std','NumStdFromMean'], axis=1)            
            
            zscore_db.loc[avg_std.index,
                             'AvgZscore_OOB_%s_days_%s_%s' %(d,t,feature)]=avg_std
            
    
    return(zscore_db)       


