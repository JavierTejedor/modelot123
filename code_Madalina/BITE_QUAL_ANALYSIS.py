def BITE_QUAL_ANALYSIS(df): 
    
    # Import useful libraries       
    import pandas as pd  
    import numpy as np 
    from datetime import timedelta   

    ###########################################################################
    ############### Transform bite qualificators decimals in binary ###########
    ###########################################################################
    
    bite_value_binary=np.unpackbits(np.uint8(df['bc']))
    bite_value_binary=np.reshape(bite_value_binary,(len(df),8))                           
    
    bite_qual_ind=['IV','SINC','OW','VH','MP','INT','AL','RES']
    
    for i in range(len(bite_qual_ind)):
        df[bite_qual_ind[i]]=bite_value_binary[:,i]
    
    df=df.drop(['RES'],axis=1)
    ###########################################################################
    ###########################################################################
    
    N_days=[15, 30, 60, 90, 180, 360, 720]
    days_to_substract=[timedelta(days=(x-1)) for x in N_days]  
    bite_alarms=['IV','SINC','OW','VH','MP','INT','AL']             
                                                    
    # Compute the first and last day of measurements for each customer
    df_range_dates = pd.DataFrame([])
    df_range_dates['fec_fin'] = df.groupby(['id','tramo'])['fec'].max()
    df_range_dates['fec_ini'] = df.groupby(['id','tramo'])['fec'].min()
    
    # Build the alarms database    
    alarms_db=pd.DataFrame([])
    
    alarms_db['id']=df_range_dates.index.get_level_values('id')
    
    alarms_db['tramo']=df_range_dates.index.get_level_values('tramo')
    
    alarms_db.index=df_range_dates.index
   
    
    for days in N_days:
        for alarms in bite_alarms:
            alarms_db['Num_%s_%s' %(alarms,days)]=np.empty(len(alarms_db))*np.nan
    
    # Join df and df_range_dates based on the index
    df=df.join(df_range_dates, on=['id','tramo'], how='right')
    
    # Add the date for 15, 30, 60, 90...days ago'
    k=2
    for i in range(len(N_days)):
        df['fec_%s_days_ago' %N_days[i]]=df['fec_fin']-days_to_substract[i]
        
        temp_df=df[(df.fec>=df['fec_%d_days_ago' %N_days[i]]
                            )&(df['fec_%d_days_ago' %N_days[i]]>=df['fec_ini'])].groupby(['id','tramo'])[bite_alarms].sum()
        df=df.drop(['fec_%s_days_ago' %N_days[i]], axis=1)
        if temp_df.shape[0]!=0:
            alarms_db.loc[temp_df.index,k:k+7]=temp_df.values
        k=k+7
        
    for alarms in bite_alarms:
        alarms_db['NumDaysFromLast_%s' %alarms]=np.zeros(len(alarms_db))
                                
    for alarms in bite_alarms:
        last_day_event=df[df['%s' %alarms]==1].groupby(['id','tramo'])['fec'].max()
        if last_day_event.shape[0]!=0:                                        
            alarms_db.loc[last_day_event.index,'NumDaysFromLast_%s' %alarms]=(
            df_range_dates.loc[last_day_event.index,'fec_fin']-last_day_event)
            
            alarms_db['NumDaysFromLast_%s' %alarms]=[x.days for x in alarms_db[
            'NumDaysFromLast_%s' %alarms]]

    return(alarms_db)