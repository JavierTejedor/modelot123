def MEAN_ENERGY_AVERAGE_RATIO(df, feature):

    # Import useful libraries
    import pandas as pd
    import numpy as np
    from national_holidays import get_national_holidays

    # Reset index
    df=df.reset_index(drop=True)

    # Sort values by id
    df=df.sort_values(by=['id'])

    # Create dummy dataframe to have the indexes of tramo and ID
    df_dates = pd.DataFrame([])
    df_dates['fec_fin'] = df.groupby(['id','tramo'])['fec'].max()
    df_dates['fec_ini'] = df.groupby(['id','tramo'])['fec'].min()

    # Create empty database for the features to be computed
    # Features = ratio between the  monthly mean energy of the last 12 months
    # and the monthly mean energy of the same month in the previous year,
    # split into L, T, S
    monthly_ratio=pd.DataFrame([])
    monthly_ratio['id']=df_dates.index.get_level_values('id')
    monthly_ratio['tramo']=df_dates.index.get_level_values('tramo')
    monthly_ratio.index=df_dates.index

    # For how many last months you want to compute the ratio?
    last_months=[1,2,3,4,5,6,7,8,9,10,11,12]
    # Type of days to compute the ratio L=0, T=1, S=2
    type_days={'L':0,'T':1,'S':2}

    for t in type_days.keys():
        for l in last_months:
            if l!=1:
                monthly_ratio['ratio_%s_%s_months_ago_%s' %(feature,l,t)]=np.empty(
                                                    len(monthly_ratio))*np.nan
            else:
                monthly_ratio['ratio_%s_%s_month_ago_%s' %(feature,l,t)]=np.empty(
                                                    len(monthly_ratio))*np.nan

    ###########################################################################
    # STEP 1:  Find incomplete months  ########################################
    ###########################################################################
    # Assign the month and year of each measurement
    df['mes']=df['fec'].dt.month
    df['ano']=df['fec'].dt.year

    # Count how many days of measurements are in each month of each year
    count_days_per_month=df.groupby(['id','tramo','mes','ano'])['fec'].count()
    count_days_per_month.name='count_days_per_month'

    # Merge the counts with the main dataframe
    df=df.join(count_days_per_month, on=['id','tramo','mes','ano'], how='left')

    ###########################################################################
    # STEP 2: Drop months with less than 28 days measurements  ################
    ###########################################################################
    df=df[df.count_days_per_month>27]

    ###########################################################################
    # STEP 3: Compute the average monthly energy in function of tipo_dia ######
    ###########################################################################
    # Create a column specifying the day of the week for each date
    df['dia_semana']=df['fec'].dt.dayofweek

    # Import national holidays
    national_holidays=get_national_holidays()

    # Categorize data in 6 types L=0, T=1, S=2, LH=3, TH=4, SH=5
    df['tipo_dia']=0
    df.loc[df.dia_semana==5,'tipo_dia']=1
    df.loc[df.dia_semana==6,'tipo_dia']=2
    df.loc[(df.tipo_dia==0) & (df.fec.isin(national_holidays)),'tipo_dia']=3
    df.loc[(df.tipo_dia==1) & (df.fec.isin(national_holidays)),'tipo_dia']=4
    df.loc[(df.tipo_dia==2) & (df.fec.isin(national_holidays)),'tipo_dia']=5

    # Get rid of the LD, TH, SH measurements
    df=df[df.tipo_dia<3]
    ae_average_month=df.groupby(['id','tramo','ano','mes','tipo_dia']
                            )[feature].mean()

    yearly_ratio=ae_average_month.unstack(level=-3)
    years=yearly_ratio.columns.values
    for i in range(max(years),min(years),-1):
        if (i in years) & (i-1 in years):
            yearly_ratio[i]=yearly_ratio[i]/yearly_ratio[(i-1)]
        else:
            yearly_ratio[i]=np.nan
    yearly_ratio=yearly_ratio.drop([min(years)],axis=1)
    yearly_ratio=yearly_ratio.stack(dropna=True)
    
    # Replace infinite values due to the division to 0 with NaNs
    yearly_ratio[np.isinf(yearly_ratio)]=np.nan
    yearly_ratio=yearly_ratio.unstack(level=-2)

    if yearly_ratio.shape==(0,0):
        monthly_ratio=pd.DataFrame([])
    else:
        yearly_ratio=yearly_ratio.sort_index(level=[0,3],ascending=False)
        yearly_ratio['id']=yearly_ratio.index.get_level_values('id')
        yearly_ratio['tramo']=yearly_ratio.index.get_level_values('tramo')

        grouped_yearly_ratio=yearly_ratio.groupby(['id','tramo'])

        for t in type_days.keys():
            for l in last_months:
                ae_month=grouped_yearly_ratio[type_days[t]].nth((l-1))
                ae_month.index=ae_month.index.droplevel([2,3])
                if ae_month.shape[0]!=0:
                    if l!=1:
                        monthly_ratio.loc[
                        ae_month.index,'ratio_%s_%s_months_ago_%s' %(feature,l,t)]=ae_month
                    else:
                        monthly_ratio.loc[
                        ae_month.index,'ratio_%s_%s_month_ago_%s' %(feature,l,t)]=ae_month

    return(monthly_ratio)
