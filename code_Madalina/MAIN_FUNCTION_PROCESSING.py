def main_function(overlap=True, path_ch='../../raw/cs_ch_chunked',
                  path_tramos='../../raw/id_tramos_chunked',
                  path_features_madalina ='../../raw/features_madalina',
                  path_alarms='alarms_db',
                  path_zscore_oob='zscore_oob_db',
                  path_trend='trend_slope_db',
                  path_monthly_ratio='monthly_ratio_db',
                  path_zero_measurements='zero_measurements'):

    # Import useful libraries
    import os
    import re
    from JOIN_TRAMOS_CHUNKS import JOIN_TRAMOS_CHUNKS
    from BITE_QUAL_ANALYSIS import BITE_QUAL_ANALYSIS
    from MEAN_ENERGY_AVERAGE_RATIO import MEAN_ENERGY_AVERAGE_RATIO
    from ZERO_MEASUREMENTS import ZERO_MEASUREMENTS
    from TREND_SLOPE import TREND_SLOPE
    from Z_SCORE_OOB import Z_SCORE_OOB
    
    os.makedirs(path_features_madalina+'/'+path_alarms, exist_ok=True)
    os.makedirs(path_features_madalina+'/'+path_zscore_oob, exist_ok=True)
    os.makedirs(path_features_madalina+'/'+path_trend, exist_ok=True)
    os.makedirs(path_features_madalina+'/'+path_monthly_ratio, exist_ok=True)
    os.makedirs(path_features_madalina+'/'+path_zero_measurements, exist_ok=True)

    # Find the name of the files
    name_ch=os.listdir(path_ch)
    name_ch_chunks=list(set([re.findall(r'\d+',x)[1] for x in name_ch]))
    name_tramos=os.listdir(path_tramos)
    name_tramos=[x for x in name_tramos if x.startswith('id')]

    ###########################################################################
    #######################   STATISTICAL ANALYSIS   ##########################
    ###########################################################################
    for number_chunk in name_ch_chunks:
        df=JOIN_TRAMOS_CHUNKS(path_ch,path_tramos,name_ch,'bc',number_chunk,overlap)
        alarms_db=BITE_QUAL_ANALYSIS(df)
        if overlap:
            alarms_db.to_csv(path_features_madalina+'/'+path_alarms+'/'+path_alarms+'_con_solape'
                                +'_%s.csv' %number_chunk, index=False)
        else:
            alarms_db.to_csv(path_features_madalina+'/'+path_alarms+'/'+path_alarms+'_sin_solape'
                                +'_%s.csv' %number_chunk, index=False)


    feature='ae'
    for number_chunk in name_ch_chunks:
        df=JOIN_TRAMOS_CHUNKS(path_ch,path_tramos,name_ch,feature,
        number_chunk, overlap)
        zero_meas_db=ZERO_MEASUREMENTS(df, feature)
        trend_db=TREND_SLOPE(df)
        if overlap:
            zero_meas_db.to_csv(path_features_madalina+'/'+path_zero_measurements+'/'+path_zero_measurements+
                    '_con_solape_%s_%s.csv' %(feature,number_chunk),index=False)
            trend_db.to_csv(path_features_madalina+'/'+path_trend+'/'+path_trend+
                    '_con_solape_%s_%s.csv' %(feature,number_chunk),index=False)
        else:
            zero_meas_db.to_csv(path_features_madalina+'/'+path_zero_measurements+'/'+path_zero_measurements+
                    '_sin_solape_%s_%s.csv' %(feature,number_chunk),index=False)
            trend_db.to_csv(path_features_madalina+'/'+path_trend+'/'+path_trend+
                    '_sin_solape_%s_%s.csv' %(feature,number_chunk),index=False)



    features_selected=['ae','f1','f2','f3','f4','f5']
    for feature in features_selected:
        print('Computing the features for: ', feature)
        for number_chunk in name_ch_chunks:
            print('Chunk number: ', number_chunk)
            df=JOIN_TRAMOS_CHUNKS(path_ch,path_tramos,name_ch,feature,
            number_chunk, overlap)
            # We do not compute the monthly ratio anymore. Uncomment to
            # compute again
            # monthly_ratio=MEAN_ENERGY_AVERAGE_RATIO(df,feature)
            zscore_oob_db=Z_SCORE_OOB(df, feature)
            if overlap:
                zscore_oob_db.to_csv(path_features_madalina+'/'+path_zscore_oob+'/'+path_zscore_oob+'_con_solape_%s_%s.csv'
                %(feature,number_chunk),index=False)
                #monthly_ratio.to_csv(path_features_madalina+'/'+path_monthly_ratio+'/'+path_monthly_ratio+
                #'_con_solape_%s_%s.csv' %(feature, number_chunk),index=False)
            else:
                zscore_oob_db.to_csv(path_features_madalina+'/'+path_zscore_oob+'/'+path_zscore_oob+'_sin_solape_%s_%s.csv'
                %(feature,number_chunk),index=False)
                #monthly_ratio.to_csv(path_features_madalina+'/'+path_monthly_ratio+'/'+path_monthly_ratio+
                #'_sin_solape_%s_%s.csv' %(feature, number_chunk),index=False)


# Call the main function to compute the features
main_function()

# Merge all the features in one file
from MERGE_FEATURES import MERGE_FEATURES
MERGE_FEATURES()    
