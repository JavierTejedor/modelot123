clustering_cnae <- function(){
  # orig <- carga_listado_original()
  # orig$sce_cli_cif <- as.character(orig$sce_cli_cif)
  # orig$sce_id_cups15 <- as.character(orig$sce_id_cups15)
  # orig$uno <- 1
  
  # AA <- orig %>% group_by(sce_cli_cnae) %>% summarise(sum(uno))
  # names(AA)[2] <- 'conteo'
  # AA$sce_cli_cnae <- gsub(' ', '', AA$sce_cli_cnae)
  # AA <- AA[AA$sce_cli_cnae!='',]
  # AA <- AA[AA$conteo>=10,]
  # AA$sce_cli_cnae <- as.integer(AA$sce_cli_cnae)
  # AA <- left_join(AA, orig[names(orig) %in% c('cs_id_pm', 'sce_cli_cnae')], by='sce_cli_cnae')
  # 
  
  AA <- contratos %>% group_by(CCNAE) %>% summarise(n())
  names(AA)[2] <- 'conteo'
  AA <- AA[AA$conteo>=10,]
  contr <- contratos[contratos$CCNAE %in% AA$CCNAE,]
  
  for (i in 1:length(atrs)){
    atr_i <<- atrs[i]
    tabla <- carga_atr()
    tabla <- tabla[tabla$id %in% contr$id,]
    if(i!=1){tabla_tot <- rbind(tabla_tot, tabla)
    }else {tabla_tot <- tabla}
  }
  rm(AA, tabla)
  
  
  tabla_tot <- left_join(tabla_tot, contratos[names(contratos) %in% c('id', 'CUPS', 'fecha', 'POT_MAX', 'CCNAE')], by='id')
  tabla_tot$cdFecfin <- as.Date(tabla_tot$cdFecfin)
  tabla_tot$fecha <- as.Date(tabla_tot$fecha)
  tabla_tot <- tabla_tot[tabla_tot$fecha<=tabla_tot$cdFecfin,]
  tabla_tot$id_tramo <- paste0(tabla_tot$id,'_',tabla_tot$tramo)
  
  ZZ <- tabla_tot %>% group_by(id_tramo) %>% summarise(max(fecha))
  names(ZZ)[2] <- 'fecha_max'
  
  tabla_tot <- left_join(tabla_tot, ZZ, by='id_tramo')
  rm(ZZ)
  tabla_tot <- tabla_tot[tabla_tot$fecha==tabla_tot$fecha_max,]
  
  
  
  # tabla_tot$num_meses <- tabla_tot$ano*12 + tabla_tot$mes
  # tabla_tot$id_tramo <- paste0(tabla_tot$id,'_',tabla_tot$tramo)
  # AA <- tabla_tot %>% group_by(id_tramo) %>% summarise(min(num_meses))
  # tabla_tot <- left_join(tabla_tot, AA, by='id_tramo')
  # tabla_tot$num_meses <- tabla_tot$num_meses - tabla_tot$`min(num_meses)`
  # tabla_tot <- tabla_tot[tabla_tot$num_meses<12,]
  # tabla_tot <- left_join(tabla_tot, orig[names(orig) %in% c('cs_id_pm', 'sce_cli_cnae', 'sce_id_cups15')], by=c('id'='cs_id_pm'))
  # tabla_tot <- left_join(tabla_tot, orig[names(orig) %in% c('cs_id_pm', 'sce_id_cups15')], by=c('id'='cs_id_pm'))
  
  
  AA <- tabla_tot %>% group_by(id_tramo) %>% summarise(mean(cdAE0mean),mean(cdAE0max))
  BB <- tabla_tot %>% group_by(CCNAE, POT_MAX) %>% summarise(mean(cdAE0mean),mean(cdAE0max))
  names(AA)[2:3] <- c('media_consu_ult_a�o','media_pot_ult_a�o')
  names(BB)[3:4] <- c('media_consu_ult_a�o_por_cnae_potmax','media_pot_ult_a�o_por_cnae_potmax')
  
  
  tabla_tot <- left_join(tabla_tot, AA, by='id_tramo')
  tabla_tot <- left_join(tabla_tot, BB, by=c('CCNAE', 'POT_MAX'))
  
  
  rm(AA,BB)
  
  tabla_tot$rel_med_consu_cnae_potmax <- tabla_tot$media_consu_ult_a�o / tabla_tot$media_consu_ult_a�o_por_cnae_potmax
  tabla_tot$rel_med_pot_cnae_potmax <- tabla_tot$media_pot_ult_a�o / tabla_tot$media_pot_ult_a�o_por_cnae_potmax
  tabla_tot <- tabla_tot[names(tabla_tot) %in% c("id_tramo","CUPS","CCNAE","POT_MAX","rel_med_consu_cnae_potmax","rel_med_pot_cnae_potmax")]
  
  
  tabla_tot <- tabla_tot %>% group_by(id_tramo) %>% summarise(min(CUPS), max(CCNAE), max(rel_med_consu_cnae_potmax), max(rel_med_pot_cnae_potmax))
  names(tabla_tot) <- c("id_tramo","CUPS","CCNAE","rel_med_consu_cnae_potmax","rel_med_pot_cnae_potmax")
  
  
  tabla_tot$id <- as.integer(gsub(pattern = '_.*' , replacement ='', tabla_tot$id_tramo))
  tabla_tot$tramo <- as.integer(gsub(pattern = '.*_' , replacement ='', tabla_tot$id_tramo))
  
  
  
  tabla_tot <- unique(tabla_tot)
  
  return(tabla_tot)}



