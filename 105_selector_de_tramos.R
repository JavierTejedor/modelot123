selectorDeTramos_iniciofinTramoIncorrecto <- function(tramos,tipoTramo){
  
  
  # Descarto tramos que no comienzan con el evento separador de tramo adecuado
  if(tipoTramo=='SinSolape'){
    numTramosIncorrectos <- tramos[eventoFecini!='fecini_cd' & eventoFecini!='fechaNormalizacionExp' & eventoFecini!='fejetdcAF_target_0' & eventoFecini!='cambioUsuario' & eventoFecini!='altaTrasInactividad',.N]
    print(paste0(numTramosIncorrectos,' tramos descartados por no ser eventoFecini ninguno de estos: fecini_cd, fechaNormalizacionExp, fejetdcAF_target_0'))
    tramos <- tramos[eventoFecini=='fecini_cd' | eventoFecini=='fechaNormalizacionExp' | eventoFecini=='fejetdcAF_target_0' | eventoFecini=='cambioUsuario' | eventoFecini=='altaTrasInactividad',]
  }else if(tipoTramo=='ConSolape'){
    numTramosIncorrectos <- tramos[eventoFecini!='fecini_cd' & eventoFecini!='fechaNormalizacionExp' & eventoFecini!='cambioUsuario' & eventoFecini!='altaTrasInactividad',.N]
    print(paste0(numTramosIncorrectos,' tramos descartados por no ser eventoFecini ninguno de estos: fecini_cd, fechaNormalizacionExp, cambioUsuario, altaTrasInactividad'))
    tramos <- tramos[eventoFecini=='fecini_cd' | eventoFecini=='fechaNormalizacionExp' | eventoFecini=='cambioUsuario' | eventoFecini=='altaTrasInactividad',]
  }
  
  # Descarto tramos que no terminan con el evento separador de tramo adecuado
  numTramosIncorrectos <- tramos[eventoFecfin!='fecfin_cd' & eventoFecfin!='fejetdcAF_target_0' & eventoFecfin!='fejetdcAF_target_1',.N]
  if(numTramosIncorrectos){
    print(paste0(numTramosIncorrectos,' tramos descartados por no ser eventoFecfin ninguno de estos: fecfin_cd, fejetdcAF_target_0, fejetdcAF_target_1'))
    tramos <- tramos[eventoFecfin=='fecfin_cd' | eventoFecfin=='fejetdcAF_target_0' | eventoFecfin=='fejetdcAF_target_1',]
  }
  
  return(tramos)
  
}

selectorDeTramos_minSizeTramo <- function(tramos,tipoTramo,paramGeneradorIdTramo){
  

  # Descarto tramos que no tienen el mínimo número de dias requeridos
  if(tipoTramo=='SinSolape'){
    numTramosIncorrectos <- tramos[as.integer(fecfin-fecini+1)<paramGeneradorIdTramo$MIN_NUM_DIAS_EXIGIDOS_PARA_TRAMO_SIN_SOLAPE_ADMISIBLE,.N]
    if(numTramosIncorrectos){
      print(paste0(numTramosIncorrectos,' tramos descartados por tener menos de ',paramGeneradorIdTramo$MIN_NUM_DIAS_EXIGIDOS_PARA_TRAMO_SIN_SOLAPE_ADMISIBLE,' dias'))
      tramos <- tramos[as.integer(fecfin-fecini+1)>=paramGeneradorIdTramo$MIN_NUM_DIAS_EXIGIDOS_PARA_TRAMO_SIN_SOLAPE_ADMISIBLE,]
    }
  }else if(tipoTramo=='ConSolape'){
    numTramosIncorrectos <- tramos[as.integer(fecfin-fecini+1)<paramGeneradorIdTramo$MIN_NUM_DIAS_EXIGIDOS_PARA_TRAMO_CON_SOLAPE_ADMISIBLE,.N]
    if(numTramosIncorrectos){
      print(paste0(numTramosIncorrectos,' tramos descartados por tener menos de ',paramGeneradorIdTramo$MIN_NUM_DIAS_EXIGIDOS_PARA_TRAMO_CON_SOLAPE_ADMISIBLE,' dias'))
      tramos <- tramos[as.integer(fecfin-fecini+1)>=paramGeneradorIdTramo$MIN_NUM_DIAS_EXIGIDOS_PARA_TRAMO_CON_SOLAPE_ADMISIBLE,]
    }
  }
  
  return(tramos)
  
}

