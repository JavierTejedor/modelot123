#library (rgeos)
#library (spatialEco)
#library (geosphere)
## Cargamos la libreria dplyr
#library(dplyr)


DistCentro <- function (dfPuntos){
  
  shpCentroides <- readOGR("./../../raw/raw_gis/Capas","centroidesMun_WGS84")
  
  #shpPuntos <- SpatialPointsDataFrame(coords = cbind(dfPuntos$LONG,dfPuntos$LAT),data = data.frame(dfPuntos = dfPuntos))
  #proj4string(shpPuntos) <- CRS ("+proj=longlat +datum=WGS84 +no_defs +ellps=WGS84")
  dfCentroide <- as.data.frame(shpCentroides[names(shpCentroides) %in% c('LONGITUD_E', 'LATITUD_ET', 'COD_INE_MO')])
  #dfPuntos <- unique(as.data.frame(dfPuntos))
  #dfPuntos <- UnirTablas(dfPuntos, dfCentroide, 'CODINE','JURISDICCI', 'RJ' )
  dfPuntosAux <- UnirTablas(dfPuntos, dfCentroide, 'CODINE','COD_INE_MO', 'RJ' )
  
  dfPuntosAux$DISTANCIA_CENTRO_MUNICIPIO_Metros <- sqrt((dfPuntosAux$LONGITUD_E - dfPuntosAux$LONG)**2 + ( dfPuntosAux$LATITUD_ET - dfPuntosAux$LAT)**2) * 100000 
  
  dfPuntosAux$`POBLACION MUNICIPIO` <- as.numeric(dfPuntosAux$`POBLACION MUNICIPIO`)
  dfPuntosAux <-  dfPuntosAux[!is.na(dfPuntosAux$`POBLACION MUNICIPIO`),]
  dfPuntosAux <-  dfPuntosAux[!is.na(dfPuntosAux$SUPERFICIE),]
  
  dfPuntosAux <-  dfPuntosAux[!is.na(dfPuntosAux$DISTANCIA_CENTRO_MUNICIPIO_Metros),]
  
  
  dfPuntosAux$DISTANCIA_CENTRO_MUNICIPIO_POBLACION <- dfPuntosAux$DISTANCIA_CENTRO_MUNICIPIO_Metros/dfPuntosAux$`POBLACION MUNICIPIO`
  dfPuntosAux$DISTANCIA_CENTRO_MUNICIPIO_SUPERFICIE <- dfPuntosAux$DISTANCIA_CENTRO_MUNICIPIO_Metros/dfPuntosAux$SUPERFICIE     
  dfPuntosAux$DENSIDAD_POBLACION <- dfPuntosAux$`POBLACION MUNICIPIO`/ dfPuntosAux$SUPERFICIE
  dfPuntosAux <- as.data.frame(dfPuntosAux)
  
  #dfPuntos$DISTANCIA_CENTRO_MUNICIPIO_Metros <- sqrt((dfPuntosAux$LONGITUD_E - dfPuntosAux$LONG)**2 + ( dfPuntosAux$LATITUD_ET - dfPuntosAux$LAT)**2) * 100000 
  #writeOGR(shpPuntos, "./Capas", layer = 'shpPTOSGZAC1', driver = 'ESRI Shapefile')
  #writeOGR(shpCentroides, "./Capas", layer = 'shpPTOSCentroides', driver = 'ESRI Shapefile')
  df2 <- UnirTablas(dfPuntos,dfPuntosAux[names(dfPuntosAux) %in% c('tramo','DISTANCIA_CENTRO_MUNICIPIO_POBLACION','DISTANCIA_CENTRO_MUNICIPIO_SUPERFICIE', 'DENSIDAD_POBLACION', 'cs_id_pm' )], "tramo|cs_id_pm", "tramo|cs_id_pm", "RJ")
  
  return (df2)
  
}

#Distancia m?s cercana a la costa

DistMasCercana <- function(dfPuntos, distribuidora)
{
  
  shpCostas <-  readOGR(paste0("./../../raw/raw_gis/Capas","/", distribuidora), "Costas")
  
  shpPuntos <- SpatialPointsDataFrame(coords = cbind(dfPuntos$LONG,dfPuntos$LAT),data = data.frame(dfPuntos = dfPuntos))
  proj4string(shpPuntos) <- CRS ("+proj=longlat +datum=WGS84 +no_defs +ellps=WGS84")
  Sys.time()
  
  PoliDist <- gDistance(shpPuntos, shpCostas, byid = TRUE)
  
  PoliDist <- apply (PoliDist, 2, function (X) min (X) [1])
  
  PoliDist <- as.data.frame(PoliDist)
  
  dfPuntos <- cbind(dfPuntos, PoliDist)
  dfPuntos$PoliDist <- dfPuntos$PoliDist * 100
  names(dfPuntos)[which(names(dfPuntos)=="PoliDist")] <- "DistCosta (km)"
  
  Sys.time()
  return (dfPuntos)
}


DistPunto <- function (puntos, df_fichTramoSolape){
  
 
  
  ## Cargamos la tabla que nos indica en que cuadrado esta cada id
  cuadrados <- puntos
  
  ## Cargamos la tabla que nos indica el resultado o la ausencia de inspecciones
  resultados <- df_fichTramoSolape
  #cuadrados <- as.data.frame(cuadrados)
  ## Nos quedamos con las variables que nos interesan
  #cuadrados <- cuadrados[names(cuadrados) %in% c("cs_id_pm","FILA","COLUMNA")]
  cuadrados <- cuadrados[,.(id,FILA,COLUMNA)]
  #names(cuadrados)[1] <- 'id'
  resultados <- resultados[,.(id,tramo,AF,fecfin)]
 # resultados <- resultados[names(resultados) %in% c("id","tramo","AF", "fecfin")]
  
  ## Nos aseguramos que todos tienen informada la fila y la columna
  cuadrados <- cuadrados[!is.na(cuadrados$FILA),]
  cuadrados <- cuadrados[!is.na(cuadrados$COLUMNA),]
  cuadrados$id <- as.integer(cuadrados$id)
  ## A?adimos a la tabla de resultados la informacion sobre el cuadrado al que pertenece el punto
  df <- left_join(resultados, cuadrados, by='id')
  #df <- UnirTablas(resultados, cuadrados, 'id', 'id', 'LJ')
  ## Borramos las tablas intemedias
  df <- as.data.table(df)
  rm(resultados, cuadrados)
  
  ## Nos quedamos con aquellos que estuvieran en la tabla de cuadrados
  df <- df[!is.na(df$FILA),]
  
  ## Creamos la variable id_tramo y borramos las columnas id y tramo
  df$id_tramo <- paste0(df$id,'_',df$tramo)
  #df$id <- NULL
  df$tramo <- NULL
  
  ## Pasamos la fecha a formato fecha
  df$fecfin <- as.Date(df$fecfin)
  
  ## Quitamos duplicados
  df <- unique(df)
  
  ## Creamos la funcion distancia a anormalidad (cuando tipo='A') o fraude (cuando tipo='F')
  distancia <- function(idd, margen_tiempo, tipo){
    id_ <- as.integer(gsub(pattern = '_.*' , replacement ='', idd))
    ## Filtramos, quedandonos con los casos que tengan fraude o anormalidad, segun el tipo, que sean distintos al id sobre el que calculamos y que tengan fecha previa a dicho id  
    AA <- df[!is.na(df$AF) & 
               (df$AF==tipo) & 
               df$id!=id_ &
               (unique(df[id_tramo==idd,fecfin]) >= df$fecfin) &
               (unique(df[id_tramo==idd,fecfin]) - df$fecfin <= margen_tiempo)
             ,]
    
    ## La funcion devuelve la distancia de un cuadro a otro en el caso de que haya ids que cumplan las condiciones anteriores y cero sino.  
    if(dim(AA)[1]==0){return(0)
    }else{return(min(sqrt((unique(df[id_tramo==idd,FILA]) - AA$FILA)**2 + (unique(df[id_tramo==idd,COLUMNA]) - AA$COLUMNA)**2)))}
    
    
  }
  
  ## Ejecutamos la funcion usando un sapply
   t0 <- Sys.time()
  #df$dist_A <- sapply(X = df$id_tramo, FUN = distancia , 100, 'A')
  df$DistFraude_KM <- sapply(X = df$id_tramo, FUN = distancia , 5000, 'F')
  df$DistAnormalidad_KM <- sapply(X = df$id_tramo, FUN = distancia , 5000, 'A')
  
  t0 <- Sys.time()
  
  ## Cambiamos id_tramo por id y tramo
  #df$id <- as.integer(gsub(pattern = '_.*' , replacement ='', df$id_tramo))
  df$tramo <- as.integer(gsub(pattern = '.*_' , replacement ='', df$id_tramo))
  df$id_tramo <- NULL 
  #df<- as.data.frame(df)
  
  
  #df <- UnirTablas(puntos, df [names(df) %in% c('tramo', 'id', 'DistAnormalidad (Km)', 'DistFraude (Km)')], "cs_id_pm|tramo", "id|tramo", "RJ")
  
  df <- UnirTablas(puntos, df [,.(tramo, id, DistFraude_KM, DistAnormalidad_KM)], "id|tramo", "id|tramo", "RJ")
  return (df)
}
