CompRural<-function(dfPuntos,  rutaCapaRural){
  
  shpPolRural<- readOGR("./Capas", rutaCapaRural)
  
  
  colnames(dfPuntos)[names(dfPuntos) == "id"]  <- 'cs_id_pm'
  #dfPuntosAux <- dfPuntos[names(dfPuntos) %in% c('sce_id_cups13', 'cs_id_pm', 'LONG', 'LAT', 'VUTMX', 'VUTMY', 'VUTMZ', 'FCAPTURA')]
  
  
  
  dfPuntosAux <- dfPuntos[, .(sce_id_cups13, cs_id_pm, LONG, LAT, VUTMX, VUTMY, VUTMZ, FCAPTURA)]
  dfPuntosAux <- dfPuntos
  shpPuntos <- SpatialPointsDataFrame(coords = cbind(dfPuntosAux$LONG,dfPuntosAux$LAT),data = data.frame(dfPuntosAux = dfPuntosAux))
  #shpPolInd <- shpPolInd[names(shpPolInd) %in% c("ID_POLYGON", "SIOSE_CODE")]
  proj4string(shpPuntos) <- CRS ("+proj=longlat +datum=WGS84 +no_defs +ellps=WGS84 +towgs84=0,0,0")
  #proj4string(shpPolInd) <- CRS (s)
  
  #puntosIntersec <- gIntersects(shpPuntos,shpPolInd, byid = TRUE)
  puntosOver <- point.in.poly(shpPuntos, shpPolRural)
  puntosOver<- as.data.table(puntosOver)
  names(puntosOver)<-gsub(pattern = '.*\\.', '', names(puntosOver))
  #puntosOver <- puntosOver[!duplicated(puntosOver$sce_id_cups13,puntosOver$cs_id_pm),]
  
  #aa <- strsplit(dimnames(puntosIntersec@coords)[[1]]," ")
  #writeOGR(shpPuntos, "./Capas", layer = 'shpPTOS2', driver = 'ESRI Shapefile')
  #writeOGR(puntosIntersec, "./Capas", layer = 'shpPTOSInt', driver = 'ESRI Shapefile')
  #  writeOGR(SHP2, "./Capas", layer = 'PolIndCZZ_2', driver = 'ESRI Shapefile')
  
 

  #df2 <- UnirTablas(dfPuntos,puntosOver[names(puntosOver) %in% c('cs_id_pm','COD_NUCPOB','NOM_NUCPOB', 'tramo')], "cs_id_pm|tramo", "cs_id_pm|tramo", "RJ")
  df2 <- UnirTablas(dfPuntos,puntosOver[,.(cs_id_pm,COD_NUCPOB,NOM_NUCPOB, tramo)], "cs_id_pm|tramo", "cs_id_pm|tramo", "RJ")
  
  #df3 <- df2[!duplicated(df2$sce_id_cups13,df2$cs_id_pm),]
  df3<- df2
  df3[!is.na(df3$COD_NUCPOB),'COD_NUCPOB'] <- "NO"
  df3[is.na(df3$COD_NUCPOB),'COD_NUCPOB'] <- "SI"
  names(df3)[which(names(df3)=="COD_NUCPOB")] <- "RURAL"
  return (df3)
  
}