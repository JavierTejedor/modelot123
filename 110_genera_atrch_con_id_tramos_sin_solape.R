
genera_atrch_con_idTramosSinSolape <- function(paths,anos_cd,chunks_a_procesar){
  

  calcula_atrcd <- function(cd){
    
    # fecini y fecfin incluidas
    setkey(cd,id,tramo,ano,mes)
    
    atrcd <- cd[,.(
      tramoTarget=unique(tramoTarget),
      tramoEventoIni=unique(tramoEventoIni),
      tramoEventoFin=unique(tramoEventoFin),
      tramoFecini=unique(tramoFecini),
      tramoFecfin=unique(tramoFecfin),
      cdFecini=min(fec),
      cdFecfin=max(fec),
      cdNumReg=.N,
      cdNum0=sum(ae==0),
      cdNumIV=sum(b7_IV),
      cdNumCA=sum(b6_CA),
      cdNumCY=sum(b5_CY),
      cdNumVH=sum(b4_VH),
      cdNumMP=sum(b3_MP),
      cdNumINT=sum(b2_INT),
      cdNumAL=sum(b1_AL),
      cdNumRES=sum(b0_RES),
      cdAE0sum=sum(as.numeric(ae)),
      cdAE0min=min(ae),
      cdAE0fmin=min(fec[which(ae==min(ae))]),
      cdAE0mean=mean(ae),
      cdAE0max=max(ae),
      cdAE0fmax=min(fec[which(ae==max(ae))]),
      cdAE0sd=sd(ae),
      cdAE1sum=sum(as.numeric(f1)),
      cdAE1min=min(f1),
      cdAE1fmin=min(fec[which(f1==min(f1))]),
      cdAE1mean=mean(f1),
      cdAE1max=max(f1),
      cdAE1fmax=min(fec[which(f1==max(f1))]),
      cdAE1sd=sd(f1),
      cdAE2sum=sum(as.numeric(f2)),
      cdAE2min=min(f2),
      cdAE2fmin=min(fec[which(f2==min(f2))]),
      cdAE2mean=mean(f2),
      cdAE2max=max(f2),
      cdAE2fmax=min(fec[which(f2==max(f2))]),
      cdAE2sd=sd(f2),
      cdAE3sum=sum(as.numeric(f3)),
      cdAE3min=min(f3),
      cdAE3fmin=min(fec[which(f3==min(f3))]),
      cdAE3mean=mean(f3),
      cdAE3max=max(f3),
      cdAE3fmax=min(fec[which(f3==max(f3))]),
      cdAE3sd=sd(f3),
      cdAE4sum=sum(as.numeric(f4)),
      cdAE4min=min(f4),
      cdAE4fmin=min(fec[which(f4==min(f4))]),
      cdAE4mean=mean(f4),
      cdAE4max=max(f4),
      cdAE4fmax=min(fec[which(f4==max(f4))]),
      cdAE4sd=sd(f4),
      cdAE5sum=sum(as.numeric(f5)),
      cdAE5min=min(f5),
      cdAE5fmin=min(fec[which(f5==min(f5))]),
      cdAE5mean=mean(f5),
      cdAE5max=max(f5),
      cdAE5fmax=min(fec[which(f5==max(f5))]),
      cdAE5sd=sd(f5)
    ),by=.(id,tramo,ano,mes)]
    
    return(atrcd)
  }  
  
  ########################################
  # Proceso
  ########################################
  
  
  for(chunk in chunks_a_procesar){
    
    # Importo id_tramos
    id_tramos <- import_id_tramos_sin_solape(chunk,paths)
    
    # Importo cd
    cd <- import_cd(paths,anos_cd,chunk)
    
    #    id        fec   ae bc  f1  f2  f3  f4  f5  ano mes b7_IV b6_CA b5_CY b4_VH b3_MP b2_INT b1_AL b0_RES
    #   169 2010-01-01  877  0 278 205 124 113 157 2010   1     0     0     0     0     0      0     0      0
    #   169 2010-01-02  864 16 284 206 125 105 144 2010   1     0     0     0     1     0      0     0      0
    #   169 2010-01-03  848  0 260 206 131 106 145 2010   1     0     0     0     0     0      0     0      0
    #   169 2010-01-04  843  0 253 211 118 105 156 2010   1     0     0     0     0     0      0     0      0
    
    #   f1: EPERIODOS (1,2,3,4,5,6,7)
    #   f2: EPERIODOS (8,9,10,11,12,13)
    #   f3: EPERIODOS (14,15,16,17)
    #   f4: EPERIODOS (18,19,20)
    #   f5: EPERIODOS (21,22,23,24,25)
    
    # Filtro con id_tramos
    cd <- cd[id %in% unique(id_tramos$id),]
    
    ##########################################
    # Division atrcd en tramos sin solape
    ##########################################
    
    setkey(id_tramos,id)
    setkey(cd,id)
    cd_tramos_sin_solape <- id_tramos[,.(id,tramo,tramoFecini=fecini,tramoFecfin=fecfin,tramoEventoIni=eventoFecini,tramoEventoFin=eventoFecfin,tramoTarget=target)][cd,allow.cartesian=T]
    rm(cd)
    gc()
    
    cd_tramos_sin_solape <- cd_tramos_sin_solape[fec>=tramoFecini & fec<=tramoFecfin,]
    
    atrcd_tramos_sin_solape <- calcula_atrcd(cd_tramos_sin_solape)
    rm(cd_tramos_sin_solape)
    gc()
    
    # Guardo atrcd_tramos_sin_solape en atrcd_id_tramos
    print('Guardo atrcd_tramos_sin_solape_xxx.csv en directorio atrcd_tramos_chunked')
    fileName <- paste0(paths$atrcd_tramos_chunked,'atrcd_tramos_sin_solape_',formatC(chunk,width=3,flag="0"),'.csv')
    write.csv2(atrcd_tramos_sin_solape,file=fileName,row.names=F)
    
  }
  return(NULL)
}





