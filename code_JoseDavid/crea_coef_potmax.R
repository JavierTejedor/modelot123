genera_coef_potmax<- function(paths_maxpot,paths_id_tramo,paths_salida,chunks){
  
  source("code_JoseDavid\\crea_tramo_potmax.R")
  
  fx<-function(dtt,l){
    
    if(dtt[,max(potmax)] == dtt[,min(potmax)] | l<12){
      return(as.numeric(NA))
    }
    
    right = dtt[, shift(.SD, 1:l, NA, "lead", TRUE),.SDcols=3]
    left = dtt[, shift(.SD, l:1, NA, "lag", TRUE),.SDcols=3]
    id_tramo = dtt[,c(1,2,3),with=FALSE]
    mean_left = left[,.(Mean = rowMeans(.SD,na.rm = TRUE))]
    mean_right = right[,.(Mean = rowMeans(.SD,na.rm = TRUE))]
    final = data.table(id_tramo[,.SD[3:(.N-2)]],(mean_left[,.SD[3:(.N-2)]]- mean_right[,.SD[3:(.N-2),Mean]])/pmax(mean_left[,.SD[3:(.N-2),Mean]],mean_right[,.SD[3:(.N-2),Mean]]))
    coef.max = final[,.(max(Mean))]
    coef.max.idx = final[,.(which.max(Mean))]
    coef.min = final[coef.max.idx[,V1]:.N,min(Mean)]
    coef = coef.max[,V1] + coef.min
    rm(right,left,mean_left,mean_right,final,coef.max,coef.max.idx,coef.min)
    gc()
    
    return(coef)
  }
  
  
  for(chunk in chunks){
    print(paste0('Feature coef_','. Procesando chunk ', formatC(chunk,width=3,flag="0"),' .Time:',Sys.time()))
      id_tramo_potmax<-crea_tramo_potencia(paths_maxpot,paths_id_tramo,chunk)
      id_tramo_potmax<-id_tramo_potmax[,.(id,tramo,potmax,maxfec_a_p0)]
    
    # id_tramo_potmax[,fec:=as.Date(fec,format="%d/%m/%Y")]
    # setkey(id_tramo_potmax,id,tramo,fec)
    # id_tramo[,i:=1:.N,by=.(id,tramo)]
    # id_tramo<-id_tramo[,.(id,tramo,i,)]
    coef<- id_tramo_potmax[,{l=.N;.(coef=fx(data.table(id,tramo,potmax),l))},by=.(id,tramo)]
    
    filename = paste0(paths_salida,"coef_potmax","\\coef_potmax","_chunk_",formatC(chunk,width=3,flag="0"),".csv")
    fwrite(coef, filename,row.names = FALSE,na=NA)
    
    rm(id_tramo_potmax)
    gc()
  }
  
  
  return(NULL)
  
}